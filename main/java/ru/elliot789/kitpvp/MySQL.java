package ru.elliot789.kitpvp;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.List;

public class MySQL {
    private static String DB_NAME;
    private static String USER;
    private static String PASS;
    private static String TABBLE;
    private static String TABBLE_KITS;
    private static Connection connection;

    public static void init() {
        Settings.log("Load settings MySQL.");
        DB_NAME = Settings.cfg.getString("MySQL.url");
        USER = Settings.cfg.getString("MySQL.username");
        PASS = Settings.cfg.getString("MySQL.password");
        TABBLE = Settings.cfg.getString("MySQL.table");
        TABBLE_KITS = Settings.cfg.getString("MySQL.tableKits");
        openConnection();
    }

    /**
     * Загрузка данных игрока
     */
    public static void loadDataPlayers(Player p) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    {
                        connection = DriverManager.getConnection(DB_NAME, USER, PASS);
                        PreparedStatement sql = connection.prepareStatement("SELECT * FROM `" + TABBLE + "` WHERE player=?");
                        sql.setString(1, p.getDisplayName());
                        ResultSet result = sql.executeQuery();
                        if (result.next()) {
                            PlayerData pd = Settings.playerData.get(p);

                            pd.kills = result.getInt("kills");
                            pd.death = result.getInt("death");

                        } else {
                            PreparedStatement np = connection.prepareStatement("INSERT INTO `" + TABBLE + "` values(?,0,0);");
                            np.setString(1, p.getDisplayName());
                            np.execute();
                            np.close();
                        }
                        sql.close();
                        result.close();
                        closeConnection();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTask(Settings.plugin);
    }

    public static void buyKit(final Kit kit, final Player p) {
        Settings.log(p.getDisplayName() + " buy Kit " + kit.name + ".");
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    connection = DriverManager.getConnection(DB_NAME, USER, PASS);
                    PreparedStatement np = connection.prepareStatement("INSERT INTO `" + TABBLE_KITS + "` values(?,?,?);");
                    np.setString(1, p.getDisplayName());
                    np.setString(2, kit.name);
                    np.setLong(3, (System.currentTimeMillis() / 1000) + kit.time);
                    np.execute();
                    np.close();
                    closeConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTask(Settings.plugin);

    }

    public static void saveOptionsPlayers(Player p) {
        new BukkitRunnable() {
            @Override
            public void run() {

                try {
                    connection = DriverManager.getConnection(DB_NAME, USER, PASS);
                    PlayerData pd = Settings.playerData.get(p);
                    {
                        PreparedStatement update = connection.prepareStatement("UPDATE `" + TABBLE + "` SET "
                                + "kills='" + (pd.kills) + "', "
                                + "death='" + pd.death + "'"
                                + " WHERE player='" + pd.p.getDisplayName() + "';");
                        update.executeUpdate();
                        update.close();
                        closeConnection();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.runTask(Settings.plugin);
    }

    /**
     * Загрузка китов
     */
    public static void loadKitsPlayers(Player p) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    connection = DriverManager.getConnection(DB_NAME, USER, PASS);
                    PlayerData pd = Settings.playerData.get(p);
                    {
                        Player p = pd.p;

                        PreparedStatement sql = connection.prepareStatement("SELECT * FROM `" + TABBLE_KITS + "` WHERE player=? AND `time` >?");

                        sql.setString(1, p.getDisplayName());
                        sql.setString(2, String.valueOf(System.currentTimeMillis() / 1000));

                        ResultSet result = sql.executeQuery();
                        while (result.next()) {
                            String nameKit = GuiMenu.kitsName.get(result.getString("kit"));
                            pd.kits.add(nameKit);
                            ItemStack item = null;
                            for (ItemStack i : pd.kitsMenu.getContents())
                                if (i != null)
                                    if (i.getItemMeta().getDisplayName() == nameKit) {
                                        item = i;
                                        break;
                                    }

                            if (item == null)
                                continue;

                            long time = result.getLong("time");
                            long timeNew = System.currentTimeMillis() / 1000;

                            ItemMeta meta = item.getItemMeta();
                            List<String> lore = meta.getLore();
                            lore.add("§aКуплено!");
                            int min = (int) (time - timeNew) / 60;
                            int chas = min / 60;
                            min = min - chas * 60;
                            if (chas == 0)
                                lore.add("§cОсталось времени:§6 " + min + " мин");
                            else
                                lore.add("§cОсталось времени:§6 " + chas + " ч " + min + " мин");
                            meta.setLore(lore);
                            item.setItemMeta(meta);
                        }
                        sql.close();
                        result.close();
                        closeConnection();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTask(Settings.plugin);
    }


    private synchronized static void openConnection() {
        try {
            connection = DriverManager.getConnection(DB_NAME, USER, PASS);
            PreparedStatement np = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `" + TABBLE + "` "
                    + "(player VARCHAR(17),"
                    + "kills INT(10),"
                    + "death INT(10));");
            np.execute();
            np.close();

            np = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `" + TABBLE_KITS + "` "
                    + "(player VARCHAR(17),"
                    + "kit VARCHAR(50),"
                    + "time BIGINT(20));");
            np.execute();
            np.close();
            closeConnection();

            Settings.log("Connect MySQL.");
        } catch (Exception e) {
            Settings.log("[ERROR] Connect MySQL error.");
            e.printStackTrace();
            Bukkit.shutdown();
        }
    }

    public synchronized static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
