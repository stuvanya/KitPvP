package ru.elliot789.kitpvp.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import ru.elliot789.eapi.utils.Redirect;
import ru.elliot789.kitpvp.GuiMenu;
import ru.elliot789.kitpvp.Settings;

import static ru.elliot789.eapi.eAPI.SERVER_LOBBY;

public class Commands implements CommandExecutor {

    // public CommandsMoney() {}
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        // COMMAND "BB"


        if (cmd.getName().equalsIgnoreCase("kitpvp")) {


            if (!(sender instanceof Player)) {
                sender.sendMessage("Нельзя писать с консоли");
                return false;
            }

            Player player = (Player) sender;

            if (!player.hasPermission("kitpvp.admin")) {
                player.sendMessage("§cНет прав.");
                return false;
            }

            // HELP MENU
            if (args.length == 0) {
                player.sendMessage("§e----------------- [KitPVP] -----------------");
                player.sendMessage("§3/kitpvp setlobby §7- установить лобби");
                player.sendMessage("§3/kitpvp setspawn §7- установить спавн спектаторов");
                player.sendMessage("§3/kitpvp tpworld §7- переместиться в мир с картой");
                player.sendMessage("§3/kitpvp clearmobs §7- убрать мобов");
                return true;
            }

            // SET LOBBY
            if (args[0].equalsIgnoreCase("setlobby")) {
                if (args.length == 1) {
                    Settings.cfg.set("lobby.x", player.getLocation().getX());
                    Settings.cfg.set("lobby.y", player.getLocation().getY());
                    Settings.cfg.set("lobby.z", player.getLocation().getZ());
                    Settings.cfg.set("lobby.look1", player.getLocation().getYaw());
                    Settings.cfg.set("lobby.look2", player.getLocation().getPitch());
                    Settings.cfg.set("lobby.world", player.getWorld().getName());
                    Settings.lobby = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(),
                            player.getLocation().getYaw(), player.getLocation().getPitch());
                    Settings.saveCfg();
                    player.sendMessage("§aЛобби установлено.");
                    return true;
                }
                player.sendMessage("§cУ этой команды 1 аргумент.");
                return false;
            }


            // Clear Mobs
            if (args[0].equalsIgnoreCase("clearmobs")) {

                if (args.length == 1) {

                    for (Entity ent : player.getWorld().getEntities()) {
                        if (!(ent instanceof Player)) {
                            ent.remove();
                        }
                    }
                    player.sendMessage("Мобы убраны");
                    return true;
                }

                player.sendMessage("§cУ этой команды 1 аргумент.");
                return false;
            }


            // TP spawn
            if (args[0].equalsIgnoreCase("lobby")) {

                if (args.length == 1) {
                    if (Settings.lobby == null) {
                        player.sendMessage("§cСпавн не установлен.");
                        return false;
                    }

                    player.teleport(Settings.lobby);
                    player.sendMessage("§aТелепортация.");
                    return true;
                }

                player.sendMessage("§cУ этой команды 1 аргумент.");
                return false;
            }


            // TP spawn
            if (args[0].equalsIgnoreCase("reload")) {

                if (args.length == 1) {
                    GuiMenu.initBegin();
                    player.sendMessage("done");
                    return true;
                }

                player.sendMessage("§cУ этой команды 1 аргумент.");
                return false;
            }


            player.sendMessage("§cТакой аргумент команды не найден.");
        }
        // COMMAND "LEAVE"
        else if (cmd.getName().equalsIgnoreCase("leave")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Нельзя писать с консоли");
                return false;
            }
            // Тпхаем игрока в лобби
            Redirect.tpServerMinOnline((Player) sender, SERVER_LOBBY);
            return true;
        } else if (cmd.getName().equalsIgnoreCase("invite")) {
            return true;
        } else if (cmd.getName().equalsIgnoreCase("")) {
            return true;
        }


        return false;
    }
}