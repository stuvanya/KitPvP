package ru.elliot789.kitpvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import ru.elliot789.kitpvp.MySQL;

public class CommandStat  implements CommandExecutor
{

    // public commands() {}
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        // COMMAND "BB"

        if (cmd.getName().equalsIgnoreCase("stat")) {

            if ((sender instanceof Player)) {


                Player player = (Player) sender;


                if (!player.hasPermission("kitpvp.admin")) {
                    player.sendMessage("§cНет прав.");
                    return false;
                }

                if (args.length == 0) {
                    player.sendMessage("Чтобы обновить статистику, введите /stat reload");
                    return true;
                }


                // SET DATA PLAYERS
                if (args[0].equalsIgnoreCase("reload")) {
                    if (args.length == 1) {

                        for (Player p : Bukkit.getOnlinePlayers()) {
                            MySQL.saveOptionsPlayers(p);
                        }

                        player.sendMessage("Save BD");
                        return true;
                    }
                    player.sendMessage("§cУ этой команды 1 аргумент.");
                    return false;
                }

            } else if (sender instanceof ConsoleCommandSender) {
                // SET DATA PLAYERS
                if (args[0].equalsIgnoreCase("reload")) {

                    if (args.length == 0) {
                        System.out.println("Чтобы обновить статистику, введите /stat reload");
                        return true;
                    }

                    if (args.length == 1) {

                        for (Player p : Bukkit.getOnlinePlayers()) {
                            MySQL.saveOptionsPlayers(p);
                        }

                        System.out.println("Save BD");
                        return true;
                    }
                    System.out.println("§cУ этой команды 1 аргумент.");
                    return false;
                }
            }
        }

        return false;
    }
}