package ru.elliot789.kitpvp.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.elliot789.kitpvp.Settings;

import java.util.ArrayList;
import java.util.List;

public class Vanish implements CommandExecutor {

    public static List<Player> vanish = new ArrayList();

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if ((sender instanceof Player)) {
            Player p = (Player) sender;
            if (cmd.getName().equalsIgnoreCase("vanish")) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage("Нельзя писать с консоли");
                    return false;
                }
                if (!p.hasPermission("minigames.vanish")) {
                    p.sendMessage("Нет прав");
                    return true;
                }


                if (vanish.contains(p)) {
                    vanish.remove(p);
                    Settings.players.add(p);
                    p.setGameMode(GameMode.SURVIVAL);

                    //Показывает игроков
                    for (Player player : Settings.players)
                        player.showPlayer(p);

                    sender.sendMessage("Режим спектатора выключен");
                } else {
                    vanish.add(p);
                    Settings.players.remove(p);
                    p.setGameMode(GameMode.SPECTATOR);

                    //Скрываем игроков
                    for (Player player : Settings.players)
                        player.hidePlayer(p);

                    sender.sendMessage("Режим спектатора включён");
                }


            }
        }
        return false;
    }

}