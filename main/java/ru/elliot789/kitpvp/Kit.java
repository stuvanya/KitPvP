package ru.elliot789.kitpvp;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Kit {
	public String name;
	public ItemStack icon;
	public String nameIcon;
	public String perm;
	public ItemStack kit[];
	public int money;
	public int time;

	Kit(FileConfiguration file, String path, Inventory inv) {
		ItemStack item = GuiMenu.loadItem(file, "kits." + path, inv);

		name = path;
		nameIcon = item.getItemMeta().getDisplayName();
		money = file.getInt("kits." + path + ".money");
		time = file.getInt("kits." + path + ".time");
		perm = "minigames." + path;

		List<String> items = file.getStringList("kits." + path + ".items");
		kit = new ItemStack[items.size()];
		int j = 0;
		for (String s : items) {
			//kit[j++] = NBT.removeAttributes(S.loadItem(s));



			kit[j++] = Settings.loadItem(s);
		}
	}
}