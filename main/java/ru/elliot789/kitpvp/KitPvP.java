package ru.elliot789.kitpvp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.elliot789.kitpvp.commands.CommandStat;
import ru.elliot789.kitpvp.commands.Commands;
import ru.elliot789.kitpvp.commands.Vanish;
import ru.elliot789.kitpvp.listener.Horse;
import ru.elliot789.kitpvp.listener.ListenersWorld;

import static ru.elliot789.eapi.languages.LanguagesAPI.loadPluginLang;

public class KitPvP extends JavaPlugin {
    //sdf
    @Override
    public void onEnable() {

        Settings.log("Check config.");
        getConfig().options().copyDefaults(true);

        loadPluginLang(Settings.plugin);

        Settings.log("Register listener.");
        Bukkit.getPluginManager().registerEvents(new Listeners(), this);
        Bukkit.getPluginManager().registerEvents(new ListenersWorld(), this);
        Bukkit.getPluginManager().registerEvents(new Horse(), this);


        Settings.log("Connect BungeeCord.");
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");


        Settings.log("Register command kitpvp.");
        getCommand("kitpvp").setExecutor(new Commands());
        getCommand("stat").setExecutor(new CommandStat());
        getCommand("leave").setExecutor(new Commands());
        getCommand("vanish").setExecutor(new Vanish());

        Settings.init();
      //  ScoreUtil.registerHealthBar();
        Tab.TabEnable();

        MySQL.init();
        saveDefaultConfig();

        GuiMenu.initBegin();


        Settings.log("KitPVP enabled.");
    }

    @Override
    public void onDisable() {
        Settings.log("KitPVP disabled.");
    }


}
