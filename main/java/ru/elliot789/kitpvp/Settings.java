package ru.elliot789.kitpvp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static ru.elliot789.eapi.languages.LanguagesAPI.getString;

public class Settings {
	//Полезные штуки
	public static Plugin plugin = Bukkit.getPluginManager().getPlugin("KitPvP");
	public static FileConfiguration cfg = Bukkit.getPluginManager().getPlugin("KitPvP").getConfig();
	public static FileConfiguration cfgWorld = Bukkit.getPluginManager().getPlugin("KitPvP").getConfig();
	public static Random r = new Random();

	//Сообщения
	public static String kill(Player p) {
		return getString(plugin, p, "message.chat.kill");
	}
	public static String death(Player p) {
		return getString(plugin, p, "message.chat.death");
	}
	public static String notkiller(Player p) {
		return getString(plugin, p, "message.chat.notkiller");
	}
	public static String noMoney(Player p) {
		return getString(plugin, p, "message.chat.noMoney");
	}
	public static String buyKit(Player p) {
		return getString(plugin, p, "message.chat.buyKit");
	}
	public static String selectKit(Player p) {
		return getString(plugin, p, "message.chat.selectKit");
	}
	public static String[] deathTitle (Player p) {
		return getString(plugin, p, "message.title.death").split(";", -2);
	}

	//Настроки игры
	public static ArrayList<String> allowedCommands = (ArrayList) cfg.getStringList("options.allowedCommands");

	public static Location lobby;
	public static ArrayList<Perm> perm = new ArrayList();

	//Настройки игрока
	public static HashMap<Player, PlayerData> playerData = new HashMap();
	public static ArrayList<Player> players = new ArrayList();
	public static ArrayList<Player> startgame = new ArrayList();


	//Предметы
	public static ItemStack bed = new ItemStack(Material.BED);
	public static int bedSlot;
	public static ItemStack kitIcon = new ItemStack(Material.BOW);
	public static int kitIconSlot;

	/**
	 * Процедура выполняется при запуске сервера.
	 */
	@SuppressWarnings("deprecation")
	public static void init() {
		log("Load lobby and spectator");

		cfgWorld = loadFile(".." + File.separator + ".." + File.separator + "world", "config.yml");

		World world = Bukkit.getServer().getWorld(cfgWorld.getString("lobby.world"));
		double x = cfgWorld.getDouble("lobby.x");
		double y = cfgWorld.getDouble("lobby.y");
		double z = cfgWorld.getDouble("lobby.z");
		System.out.println(cfgWorld.getDouble("lobby.z"));
		float look1 = (float) cfgWorld.getDouble("lobby.look1");
		float look2 = (float) cfgWorld.getDouble("lobby.look2");
		lobby = new Location(world, x, y, z, look1, look2);

		log("Load items.");
		bed.setItemMeta(setDisplayNameItem(cfg.getString("items.bed.name"), bed));
		bedSlot = cfg.getInt("items.bed.slot") - 1;
		kitIcon.setItemMeta(setDisplayNameItem(cfg.getString("items.kit.name"), kitIcon));
		kitIconSlot = cfg.getInt("items.kit.slot") - 1;

		log("Load permissions.");
		for (String list : cfg.getConfigurationSection("donate").getKeys(false))
			perm.add(new Perm("donate." + list + '.'));
	}



	/**
	 * Установить имя предмету
	 */
	private static ItemMeta setDisplayNameItem(String name, ItemStack item) {
		name = replace(name);
		ItemMeta m = item.getItemMeta();
		m.setDisplayName(name);
		return m;
	}

	/**
	 * Сохранить конфиг
	 */
	public static void saveCfg() {
		plugin.saveConfig();
	}



	/**
	 * Logger
	 */
	public static void log(String msg) {
		Bukkit.getLogger().info("[KitPVP] " + msg);
	}

	/**
	 * Открываем файл
	 */
	public static FileConfiguration loadFile(String path, String fileName) {
		String separator = "";
		if (path != "")
			separator = File.separator;

		Settings.log("Load file " + path + separator + fileName);
		File file = new File(plugin.getDataFolder() + separator + path, fileName);

		if (!file.exists()) {
			try {
				plugin.saveResource(path + separator + fileName, false);
			} catch (Exception e) {
				e.printStackTrace();
				Settings.log("Couldn't save " + path + separator + fileName + " to disk!");
				Bukkit.shutdown();
				return null;
			}
		}
		return YamlConfiguration.loadConfiguration(file);
	}

	/**
	 * Цветовая замена
	 */
	public static String replace(String msg) {
		return msg.replace('&', '§');
	}

	/**
	 * Загрузка предмета из строки
	 */
	@SuppressWarnings("deprecation")
	public static ItemStack loadItem(String s) {
		String item[] = s.split(":", -3);
		if (item[1].length() == 0)
			item[1] = "0";
		else if (item[1].contains("RAND")) {
			item[1] = rand(item[1]);
		}

		if (item[2].length() == 0)
			item[2] = "1";
		else if (item[2].contains("RAND")) {
			item[2] = rand(item[2]);
		}

		ItemStack newItem = new ItemStack(Material.getMaterial(Integer.parseInt(item[0])), Integer.parseInt(item[2]), (short) Integer.parseInt(item[1]));

		if (item.length == 4) {
			String ench[] = item[3].split(";");
			for (int i = 0; i < ench.length; i++) {
				String e[] = ench[i].split(",");
				if (e[0].contains("RAND"))
					e[0] = rand(e[0]);
				if (e[1].contains("RAND"))
					e[1] = rand(e[1]);
				ItemMeta meta = newItem.getItemMeta();
				meta.addEnchant(Enchantment.getById(Integer.parseInt(e[0])), Integer.parseInt(e[1]), true);
				newItem.setItemMeta(meta);
			}
		}
		return newItem;
	}

	/**
	 * Рандом в конфиге
	 *
	 * @param s команда
	 */
	private static String rand(String s) {
		s = s.replace("RAND(", "").replace(")", "");
		String rand[] = s.split("-");
		int otInt = Integer.parseInt(rand[0]);
		int doInt = Integer.parseInt(rand[1]);
		s = "" + (Settings.r.nextInt(doInt - otInt + 1) + otInt);
		return s;
	}

	/**
	 * Дистанция от игрока к игроку
	 *
	 * @param p   первый игрок
	 * @param toP второй
	 * @return дистанция
	 */
	public static float checkDistanse(Player p, Player toP) {
		double x = p.getLocation().getX() - toP.getLocation().getX();
		double y = p.getLocation().getY() - toP.getLocation().getY();
		double z = p.getLocation().getZ() - toP.getLocation().getZ();

		return (float) Math.sqrt(x * x + y * y + z * z);
	}

}
