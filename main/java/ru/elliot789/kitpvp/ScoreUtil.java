package ru.elliot789.kitpvp;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import ru.elliot789.eapi.economy.EconomyAPI;

import java.util.ArrayList;
import java.util.HashMap;

public class ScoreUtil {


    public static HashMap<Player, ArrayList<String>> scoreTextPlayer = new HashMap(); //Scoreboard для каждого игрока

    private static int tickAnim = 0;
    private static String anim[] =
            {
                    "§e§lKitPvP",
                    "§f§lK§6§li§e§ltPvP",
                    "§f§lKi§6§lt§e§lPvP",
                    "§f§lKit§6§lP§e§lvP",
                    "§f§lKitP§6§lv§e§lP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
                    "§f§lKitPvP",
            };

    private static BukkitRunnable timer = null;

    /**
     * Процедура запускается при старте
     */
    public static void init() {
        Settings.log("Load scoreboard settings.");
        aninationTitle(); //Включаем аниамцию
    }

    public static void initBegin() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            Scoreboard s = p.getScoreboard();
            Objective heal = s.registerNewObjective("showhealth", "health");
            heal.setDisplaySlot(DisplaySlot.BELOW_NAME);
            heal.setDisplayName("§c❤");

            Objective tab = s.registerNewObjective("scoreTab", "dummy");
            tab.setDisplaySlot(DisplaySlot.PLAYER_LIST);
        }
        //Обновляем
        for (Player p : Bukkit.getOnlinePlayers())
            for (Player p1 : Bukkit.getOnlinePlayers())
                p.getScoreboard().getObjective("showhealth").getScore(p1.getDisplayName()).setScore(20);
    }


    public static void registerHealthBar() {
        Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
        Objective heal = s.registerNewObjective("showhealth", "health");
        heal.setDisplaySlot(DisplaySlot.BELOW_NAME);
        heal.setDisplayName("§c❤");
    }

    /**
     * Анимация scoreboard
     */
    private static void aninationTitle() {
        timer = new BukkitRunnable() //Анимация тайтла
        {
            @Override
            public void run() //Гл. процедура таймера
            {
                for (Player online : Bukkit.getOnlinePlayers())
                    online.getScoreboard().getObjective("test").setDisplayName(anim[tickAnim]);
                tickAnim++;
                if (tickAnim == anim.length)
                    tickAnim = 0;
            }
        };
        timer.runTaskTimer(Settings.plugin, 0L, 3L); //20L = 1 секунда (период)
    }

    /**
     * Обновить у игрока скоребоард
     */
    public static void lobbyScore(Player p) {
        scoreTextPlayer.put(p, new ArrayList());

        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective pObjective = board.registerNewObjective("test", "dummy");
        pObjective.setDisplaySlot(DisplaySlot.SIDEBAR); //регистрируем объекты
        pObjective.setDisplayName(anim[0]); //Указываем тип скоребоард
        p.setScoreboard(board);

        setSize(p, 6);
        editLine(p, 0, " ");
        editLine(p, 1, "Для начала игры");
        editLine(p, 2, "выберите набор");
        editLine(p, 3, "  ");
        editLine(p, 4, "Монет: §e" + EconomyAPI.getCoins(p));
        editLine(p, 5, "Уровень: §e" + EconomyAPI.getLevel(p) + " | " + EconomyAPI.getProcent(p) + "%");
        editLine(p, 6, "   ");
        //editLine(p, 6, "Карта: §e" + S.cfgWorld.getString("lobby.world"));
        //editLine(p, 6, "    ");
    }


    /**
     * Обновить у игрока скоребоард
     */
    public static void gameScore(Player p) {
        PlayerData pd = Settings.playerData.get(p);

        setSize(p, 8);
        editLine(p, 0, " ");
        editLine(p, 1, "Набор: §e" + pd.selectKitName);
        editLine(p, 2, "  ");
        editLine(p, 3, "Убийств: §e" + pd.kills);
        editLine(p, 4, "Смертей: §e" + pd.death);
        editLine(p, 5, "   ");
        editLine(p, 6, "Монет: §e" + EconomyAPI.getCoins(p));
        editLine(p, 7, "Уровень: §e" + EconomyAPI.getLevel(p) + " | " + EconomyAPI.getProcent(p) + "%");
        editLine(p, 8, "    ");
    }

    /**
     * Удаление scoreboard
     */
    public static void removePlayer(Player p) {
        clearPlayer(p);
        p.getScoreboard().getObjective("test").unregister();
        scoreTextPlayer.remove(p);
    }

    /**
     * Редактировать строку всем игрокам
     */
    public static void editLine(int line, String newLine) {
        for (Player p : Bukkit.getOnlinePlayers())
            editLine(p, line, newLine);
    }

    /**
     * Убрать строку всем
     */
    public static void resetLine(String line) {
        for (Player p : Bukkit.getOnlinePlayers())
            resetLine(p, line);
    }

    /**
     * Убрать строку
     */
    public static void resetLine(Player p, String line) {
        p.getScoreboard().resetScores(line);
        scoreTextPlayer.get(p).remove(line);
    }

    /**
     * Редактировать строку игрока
     */
    public static void editLine(Player p, int line, String newLine) {
        ArrayList<String> ar = scoreTextPlayer.get(p);
        if (ar.size() <= line) {
            return;
        }
        String s1 = ar.get(line);
        String s2 = checkSize(newLine, p.getScoreboard());
        ar.set(line, s2);
        p.getScoreboard().resetScores(s1);
        p.getScoreboard().getObjective("test").getScore(s2).setScore(ar.size() - line - 1); // Регистрируем новый ник
    }


    /**
     * Установить размер
     */
    public static void setSize(int size) {
        for (Player player : Bukkit.getOnlinePlayers())
            setSize(player, size);
    }

    /**
     * Установить размер
     */
    public static void setSize(Player player, int size) {
        ArrayList<String> ar = scoreTextPlayer.get(player);
        while (ar.size() < size + 1) {
            String s = nullString(ar.size());
            ar.add(s);
            player.getScoreboard().getObjective("test").getScore(s).setScore(ar.size()); //Регистрируем новый ник
        }
    }

    /**
     * Пустая строка
     */
    private static String nullString(int nomer) {
        String s = "" + nomer;
        String end = "";
        for (int i = 0; i < s.length(); i++)
            end += "§" + s.charAt(i);
        return end;
    }

    /**
     * Удалить Scoreboard у всех игроков
     */

    public static void clearPlayer() {
        for (Player p : Bukkit.getOnlinePlayers())
            clearPlayer(p);
    }

    /**
     * Удалить Scoreboard у игрока
     */
    public static void clearPlayer(Player p) {
        for (Team t : p.getScoreboard().getTeams())
            t.unregister();
        ArrayList<String> ar = scoreTextPlayer.get(p);

        for (String s : ar)
            p.getScoreboard().resetScores(s);

        ar.clear();
    }

    /**
     * Фикс размера строки
     */
    private static String checkSize(String score, Scoreboard board) {
        String string = "";
        if (score.length() <= 16) {
            string = score;
        } else if (score.length() <= 32) {

            string = score.substring(0, 16);
            if (board.getTeam(string) != null)
                board.getTeam(string).unregister();
            Team team = board.registerNewTeam(string);
            team.addEntry(string);
            team.setSuffix(score.substring(16));
        } else if (score.length() <= 48) {
            string = score.substring(16, 32);
            if (board.getTeam(string) != null)
                board.getTeam(string).unregister();
            Team team = board.registerNewTeam(string);
            team.setPrefix(score.substring(0, 16));
            team.addEntry(string);
            team.setSuffix(score.substring(32));
        }
        return string;
    }

}