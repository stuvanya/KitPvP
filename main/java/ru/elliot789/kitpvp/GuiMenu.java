package ru.elliot789.kitpvp;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.elliot789.eapi.economy.EconomyAPI;

import java.util.HashMap;
import java.util.List;

public class GuiMenu {

    public static Inventory menuKits;
    public static ItemStack[] defaultKit;
    public static HashMap<String, Kit> kits = new HashMap();
    public static HashMap<String, String> kitsName = new HashMap();

    public static void initBegin() {
        // Загрузка китов
        Settings.log("Load kits gui menu.");
        FileConfiguration file = Settings.loadFile("gui", "kits.yml");
        menuKits = Bukkit.createInventory(null, 9 * file.getInt("size"), Settings.replace(file.getString("name")));
        for (String list : file.getConfigurationSection("kits").getKeys(false)) {
            Kit k = new Kit(file, list, menuKits);
            kits.put(k.nameIcon, k);
            kitsName.put(k.name, k.nameIcon);
        }
        for (String itemm : kits.keySet()) System.out.println(itemm);

        defaultKit = kits.get(file.getString("default")).kit;
    }

    /**
     * Считать настройки предмета и занести его в указанный инвентарь
     *
     * @param file      файл
     * @param path      путь в предмету
     * @param inventory инфентарь
     */
    public static ItemStack loadItem(FileConfiguration file, String path, Inventory inventory) {
        path += '.';
        ItemStack item;
        if (!file.contains(path + "data"))
            item = new ItemStack(Material.getMaterial(file.getString(path + "item")));
        else
            item = new ItemStack(Material.getMaterial(file.getString(path + "item")), 1, (short) file.getInt(path + "data"));
        ItemMeta m = item.getItemMeta();
        m.setDisplayName(file.getString(path + "name").replace('&', '§'));
        List<String> list = file.getStringList(path + "lore");
        for (int i = 0; i < list.size(); i++)
            list.set(i, Settings.replace(list.get(i)));
        m.setLore(list);
        m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(m);

        setItem(file.getInt(path + "x"), file.getInt(path + "y"), item, inventory);
        return item;
    }

    /**
     * boolean предмет
     *
     * @param file      файл
     * @param path      уть
     * @param inventory инвентарь
     * @param value     значение по умолчанию
     * @return
     */
    private static ItemStack[] loadItemBool(FileConfiguration file, String path, Inventory inventory, boolean value) {
        path += '.';
        ItemStack itemENABLED;
        if (!file.contains(path + "data"))
            itemENABLED = new ItemStack(Material.getMaterial(file.getString(path + "item")));
        else
            itemENABLED = new ItemStack(Material.getMaterial(file.getString(path + "item")), 1, (short) file.getInt(path + "data"));
        ItemMeta m = itemENABLED.getItemMeta();
        m.setDisplayName(file.getString(path + "name").replace('&', '§'));
        itemENABLED.setItemMeta(m);

        ItemStack itemDISABLED = itemENABLED.clone();

        List<String> loreENABLED = file.getStringList(path + "loreENABLED");
        List<String> loreDISABLED = file.getStringList(path + "loreDISABLED");
        for (int i = 0; i < loreENABLED.size(); i++)
            loreENABLED.set(i, Settings.replace(loreENABLED.get(i)));
        for (int i = 0; i < loreDISABLED.size(); i++)
            loreDISABLED.set(i, Settings.replace(loreDISABLED.get(i)));

        m = itemENABLED.getItemMeta();
        m.setLore(loreENABLED);
        m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        itemENABLED.setItemMeta(m);

        m = itemDISABLED.getItemMeta();
        m.setLore(loreDISABLED);
        m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        itemDISABLED.setItemMeta(m);

        if (value)
            setItem(file.getInt(path + "x"), file.getInt(path + "y"), itemENABLED, inventory);
        else
            setItem(file.getInt(path + "x"), file.getInt(path + "y"), itemDISABLED, inventory);
        ItemStack items[] =
                {itemENABLED, itemDISABLED};
        return items;
    }

    /**
     * Установить спредмет в гуи меню
     *
     * @param x    кооордината
     * @param y    кооордината
     * @param item предмет
     * @param inv  инвентарь
     */
    public static void setItem(int x, int y, ItemStack item, Inventory inv) {
        inv.setItem((x - 1) + (y - 1) * 9, item);
    }

    /**
     * Нажатие в инвентаре
     *
     * @param event эвент
     */
    public static void onInventoryClickEvent(InventoryClickEvent event) {
        ItemStack item = event.getCurrentItem();
        Player p = (Player) event.getWhoClicked();
        PlayerData pd = Settings.playerData.get(p);

        if (event.getInventory().equals(pd.kitsMenu)) {
            String itemName = item.getItemMeta().getDisplayName();
            if (kits.containsKey(itemName)) {
                Kit k = kits.get(itemName);
                if (!pd.kits.contains(itemName)) {
                    if (EconomyAPI.getCoins(p.getPlayer()) < k.money) p.sendMessage(Settings.noMoney(p));
                    else {
                        ItemMeta meta = item.getItemMeta();
                        List<String> lore = meta.getLore();
                        lore.add("§aКуплено!");
                        meta.setLore(lore);
                        item.setItemMeta(meta);
                        MySQL.buyKit(k, p);
                        pd.kits.add(k.nameIcon);
                        p.sendMessage(Settings.buyKit(p));
                        EconomyAPI.takeCoins(p.getDisplayName(), k.money);
                        ScoreUtil.editLine(p, 4, "Монет: §e" + EconomyAPI.getCoins(p));
                    }
                } else {
                    pd.selectKitName = item.getItemMeta().getDisplayName();
                    pd.selectKit = kits.get(itemName).kit;
                    p.sendMessage(Settings.selectKit(p).replace("%name%", item.getItemMeta().getDisplayName()));
                }
            }
            p.closeInventory();
            event.setCancelled(true);
        }
    }

    /**
     * Копия инвентаря
     *
     * @param inv инвентарь, которого копируют
     * @return скопированный инвентарь
     */
    public static Inventory cloneInventory(Inventory inv) {
        Inventory inv1 = Bukkit.createInventory(null, inv.getSize(), inv.getName());
        inv1.setContents(inv.getContents());
        return inv1;
    }
}
