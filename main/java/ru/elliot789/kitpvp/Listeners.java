package ru.elliot789.kitpvp;

import org.bukkit.event.Listener;
import ru.elliot789.eapi.api.PexFormatter;
import ru.elliot789.eapi.minigames.titleapi.TitleAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import ru.elliot789.eapi.utils.Redirect;
import ru.elliot789.kitpvp.commands.Vanish;

import static ru.elliot789.eapi.eAPI.SERVER_LOBBY;

public class Listeners implements Listener {
    boolean isErwartungServer = true;

    /**
     * TODO Вход на сервер
     */
    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        if (isErwartungServer) {
            isErwartungServer = false;
            ScoreUtil.init();
        }

        Player p = event.getPlayer(); //Игрок
        //  p.getScoreboard().getObjective("showhealth").getScore(p.getDisplayName()).setScore(20);

        Settings.playerData.put(p, new PlayerData(p));
        Settings.players.add(p);

        Game.lobbyMode(p);
        //  ScoreUtil.initBegin();

        MySQL.loadDataPlayers(p);
        MySQL.loadKitsPlayers(p);

        p.teleport(Settings.lobby); //Телепортируем на спавн

        //Скрываем хелперов
        if (Vanish.vanish.size() != 0)
            for (Player player : Settings.players)
                for (Player vanish : Vanish.vanish)
                    player.hidePlayer(vanish);


        event.setJoinMessage(null); //Убираем приветсвенное сообщение
    }

    /**
     * TODO Выход из сервера
     */
    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        //if (Bukkit.getOnlinePlayers().size() - 1 == 0) //Если Осталось 0 игроков
        //	Bukkit.shutdown(); //Тогда оффаем сервер

        Player p = event.getPlayer(); //Игрок
        PlayerData pd = Settings.playerData.get(p);

        Settings.playerData.remove(p, new PlayerData(p));
        Settings.players.remove(p);
        ScoreUtil.removePlayer(p); //Удаляем игрока из обновления scoreboard

        MySQL.saveOptionsPlayers(p);

        event.setQuitMessage(null); //Убираем стандартное сообщение о смерти

    }

    /**
     * TODO Нажати в инвентаре
     */
    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {

        Player p = (Player) event.getWhoClicked();

        if (event.getClick().isCreativeAction()) //Если игрок кликнул в креатив меню (Ну а мало ли?)
            return; //Тогда выходим
        if (event.getCurrentItem() == null) //Если кликнул по пустой ячейке или вообще по текстуре
            return; //Тогда выходим

        if (event.getCurrentItem().equals(Settings.bed)) //Если игрок кликнул по кровати
        {
            event.setCancelled(true); //Отмена события
            return; //Выходим
        }
        if (!Settings.startgame.contains(p)) {
            if (event.getCurrentItem().equals(Settings.kitIcon)) //Если игрок кликнул по киту
            {
                event.setCancelled(true); //Отмена события
                return; //Выходим
            }
        }
        GuiMenu.onInventoryClickEvent(event); //Проверяем на нажатие возможных инвентарей
    }

    /**
     * TODO Взаимодействие
     */
    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        Player p = event.getPlayer(); //Игрок

        if (!Settings.startgame.contains(p)) {
            event.setCancelled(true);
        }
        if (!event.hasItem()) //Если игрок кликнул рукой, тогда дальше проверять нечего
            return;

        ItemStack item = event.getItem(); //Получаем предмет
        //Кровать (выход в лобби)
        if (item.equals(Settings.bed)) {
            Redirect.tpServerMinOnline(p, SERVER_LOBBY); //Тп в рандомное лобби
        }
        //Меню китов
        if (!Settings.startgame.contains(p)) {
            if (item.equals(Settings.kitIcon)) {
                p.openInventory(Settings.playerData.get(p).kitsMenu); //Открываем меню китов
            }
        }
    }


    /**
     * TODO сметь игрока
     */
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent event) {
        event.setDeathMessage(null); //Убираем сообщение смерти
        event.getDrops().clear();

        Player p = event.getEntity(); //Энтити умершего
        Player k = p.getKiller(); //Энтити киллера


        String kill = null; //Новое сообщение о смерти
        String death = null; //Новое сообщение о смерти

        //Если убийцы нету или убийца не игрок
        if (k == null || !(k instanceof Player)) {
            death = Settings.notkiller(p).replace("%player%", PexFormatter.getPrefix(p) + p.getDisplayName()); //тогда это сообщение
        }
        //иначе убийца есть
        else {
            //сообщение будет это
            kill = Settings.kill(p).
                    replace("%player%", PexFormatter.getPrefix(p) + p.getDisplayName()).
                    replace("%killer%", PexFormatter.getPrefix(k) + k.getDisplayName());
            death = Settings.death(p).
                    replace("%player%", PexFormatter.getPrefix(p) + p.getDisplayName()).
                    replace("%killer%", PexFormatter.getPrefix(k) + k.getDisplayName());

            k.sendMessage(kill); //Сообщение о смерти
        }

        p.sendMessage(death); //Сообщение о смерти


        PlayerData pdKill = Settings.playerData.get(k);
        PlayerData pdDeath = Settings.playerData.get(p);

        pdKill.addKill();
        pdDeath.addDeath();

        TitleAPI.sendTitle(p, 20, 50, 20, Settings.deathTitle(p)[0], Settings.deathTitle(p)[1]); //Выводим сообщение смерти в тайтл
    }
}
