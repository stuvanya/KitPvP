package ru.elliot789.kitpvp.listener;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Horse implements Listener {

    HashMap<Player, org.bukkit.entity.Horse> blah = new HashMap();
    @EventHandler
    public void onInteractHorse(PlayerInteractEvent e) {
        if ((e.getPlayer().getItemInHand() != null) &&
                (e.getPlayer().getItemInHand().getType() == Material.GOLD_BARDING)) {
            Player player = e.getPlayer();

            org.bukkit.entity.Horse horse = player.getWorld().spawn(player.getLocation(),
                    org.bukkit.entity.Horse.class);
            org.bukkit.entity.Horse entityHorse = horse;
            entityHorse.getInventory()
                    .setSaddle(new ItemStack(Material.SADDLE));

            horse.setOwner(player);
            //   horse;

            horse.setAdult();
            horse.setPassenger(player);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0F,
                    1.0F);

            blah.put(player, horse);
            e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
        }
    }

    @EventHandler
    public void onPlayerHorseDeath(PlayerDeathEvent e) {
        Player player = e.getEntity();
        if (blah.get(player) != null) {
            org.bukkit.entity.Horse h = blah.get(player);
            h.remove();
        }
    }

    @EventHandler
    public void onPlayerHorseQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        if (blah.get(player) != null) {
            org.bukkit.entity.Horse h = blah.get(player);
            h.remove();
        }
    }

    @EventHandler
    public void onPlayerHorseKick(PlayerKickEvent e) {
        Player player = e.getPlayer();
        if (blah.get(player) != null) {
            org.bukkit.entity.Horse h = blah.get(player);
            h.remove();
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (event.getRightClicked() instanceof org.bukkit.entity.Horse) {
            Player damager = event.getPlayer();
            org.bukkit.entity.Horse h = (org.bukkit.entity.Horse) event.getRightClicked();
            if ((blah.get(damager) == null) || (h != blah.get(damager))) {
                damager.sendMessage("§cЭто не ваша лошадь!");
                event.setCancelled(true);
            }
        }

    }
}
