package ru.elliot789.kitpvp.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import ru.elliot789.kitpvp.commands.Vanish;
import ru.elliot789.kitpvp.Game;
import ru.elliot789.kitpvp.Settings;

public class ListenersWorld implements Listener {


    /**
     * TODO command
     */
    @EventHandler
    public void blockCommand(PlayerCommandPreprocessEvent e) {
        Player player = e.getPlayer();

        if (!player.isOp() || player.hasPermission("minigames.admin")) {
            return;
        }

        String command = e.getMessage().split(" ")[0].replace("/", "").toLowerCase();
        if (!Settings.allowedCommands.contains(command)) {
            e.setCancelled(true);
            player.sendMessage("§cКоманды на арене запрещены, чтобы выйти с арены,");
            player.sendMessage("§cпропишите §c/leave");
        }
    }

    /**
     * TODO food
     */
    @EventHandler
    public void onLoseHungerwhileInGrace(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    /**
     * TODO drop
     */
    @EventHandler
    public void drop(PlayerDropItemEvent event) {

        event.setCancelled(true);
    }


    /**
     * TODO pick up item
     */
    @EventHandler
    public void onPlayerPickupItemEvent(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }


    /**
     * TODO Урон
     */
    @EventHandler
    public void onEntityDamageEvent(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) //Если урон получил не игрок, тогда пофиг
            return;
        if (!Settings.startgame.contains(event.getEntity())) //Если урон получил наблюдатель
            event.setCancelled(true); //Тогда отменяем событие
    }

    /**
     * TODO пвп
     */
    //  @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {

        Entity damager = event.getDamager(); //Энтити дамагера
        Entity entity = event.getEntity();   //Энтити игрока, который получил урон

        if (!Settings.startgame.contains(damager) || !Settings.startgame.contains(entity)) //Если дамагер наблюдатель
        {
            event.setCancelled(true); //Тогда отменяем удар
        }
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    void death(PlayerDeathEvent e) {
        final Player p = e.getEntity();
        {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Settings.plugin, new Runnable() {
                public void run() {
                    p.spigot().respawn();
                    p.setCanPickupItems(false);
                    Settings.startgame.remove(p);
                    Game.lobbyMode(p);


                }
            }, 5L);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void respawn(PlayerRespawnEvent e) {
        e.getPlayer().setCanPickupItems(true);
        e.setRespawnLocation(Settings.lobby);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (((event.getEntity() instanceof Player)) &&
                (event.getCause() == EntityDamageEvent.DamageCause.FALL)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void startGame(PlayerMoveEvent event) {

        Player player = event.getPlayer();
        int y = player.getLocation().getBlockY();

        if (Vanish.vanish.contains(player)) {
            return;
        }

        if (Settings.startgame.contains(player)) {
            return;
        }

        if (y < 100) {
            Settings.startgame.add(player);
            Game.gameMode(player);
        }
    }


    @EventHandler
    public void onBreak(BlockBreakEvent e) {

        if (e.getPlayer().hasPermission("e.Admin")) {
            return;
        }

        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {

        if (e.getPlayer().hasPermission("e.Admin")) {
            return;
        }

        e.setCancelled(true);
    }

    @EventHandler
    public void Item(PlayerInteractEvent e) {
        if (!(e.getAction() == Action.RIGHT_CLICK_AIR)) return;
        if (!(e.getItem().getType() == Material.HOPPER)) return;
        e.setCancelled(true);
    }

//	@EventHandler
//	public void onDurabilidade(EntityDamageByEntityEvent e)
//	{
//		if (((e.getEntity() instanceof Player)) && ((e.getDamager() instanceof Player)))
//		{
//			Player p = (Player)e.getDamager();
//				p.getItemInHand().setDurability((short)-p.getItemInHand().getType().getMaxDurability());
//
//
//			ItemStack[] armor = p.getInventory().getArmorContents();
//			if (armor.length > 0) {
//				for (int i = 0; i < armor.length; i++) {
//						armor[i].setDurability((short)-armor[i].getType().getMaxDurability());
//
//				}
//			}
//		}
//	}

}