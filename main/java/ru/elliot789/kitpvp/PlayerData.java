package ru.elliot789.kitpvp;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.elliot789.eapi.economy.EconomyAPI;

import java.util.ArrayList;

public class PlayerData
{
	public Perm perm;
	
	public Inventory kitsMenu;
	public ArrayList<String> kits = new ArrayList();
	public ItemStack[] selectKit;
	public String selectKitName = "Стандартный";

	private int earnedCoins = 0;
	public Player p;
	
	public int death;
	public int kills;
	public int kill = 0;


	PlayerData(Player p)
	{
		this.p = p;

		//for (Perm pr : S.perm) {
		//	if (!p.hasPermission(pr.permission)) {
		//		{
		//			continue;
		//		}
		//	}
		//	else
		//	{
		//		perm = pr;
		//	}
		//}

		Settings.perm.forEach(pr -> {
			if(p.hasPermission(pr.permission)){
				perm = pr;
			}
		});


		//S.perm.stream().filter(pr -> p.hasPermission(pr.permission)).map(perm = pr);

		kitsMenu = GuiMenu.cloneInventory(GuiMenu.menuKits);
	}


	public void addCoins(int coints)
	{
		earnedCoins += coints;
	}
	
	public int getCoints()
	{
		return earnedCoins;
	}
	
	public void addKill()
	{
		kills++;
	//	PlayerData pd = S.playerData.get(p);
        kill++;

		EconomyAPI.addCoins(p.getDisplayName(), perm.moneyKill);
		EconomyAPI.addRanks(p.getDisplayName(), perm.expKill);

		ScoreUtil.editLine(p, 3, "Убийств: §e" + kills);
		ScoreUtil.editLine(p, 6, "Монет: §e" + EconomyAPI.getCoins(p));
		ScoreUtil.editLine(p, 7, "Уровень: §e" + EconomyAPI.getLevel(p) + " | " + EconomyAPI.getProcent(p) + "%");

	//	for (Player pl : Bukkit.getOnlinePlayers()) //редактируем таб
	//		pl.getScoreboard().getObjective("scoreTab").getScore(p.getDisplayName()).setScore(kill);
	}


	public void addDeath()
	{
		death++;
		ScoreUtil.editLine(p, 4, "Смертей: §e" + death);
	}

}
