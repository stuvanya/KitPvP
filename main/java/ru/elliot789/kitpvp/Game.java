package ru.elliot789.kitpvp;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;


public class Game {

    public static void lobbyMode(Player p) {
        clearInv(p);

        ScoreUtil.lobbyScore(p); //Присваиваем игроку статистику

        p.getInventory().setItem(Settings.bedSlot, Settings.bed); //Выдаем кровать
        p.getInventory().setItem(Settings.kitIconSlot, Settings.kitIcon);
    }


    public static void gameMode(Player p) {
        PlayerData pd = Settings.playerData.get(p);
        p = pd.p;

        clearInv(p);

        ScoreUtil.clearPlayer(p);
        ScoreUtil.gameScore(p);

        p.getInventory().setItem(Settings.bedSlot, Settings.bed); //Выдаем кровать

        //ВЫДАЕМ КИТ
        if (pd.selectKit != null) {
            for (ItemStack item : pd.selectKit)
                p.getInventory().addItem(item);
        } else {
            for (ItemStack item : GuiMenu.defaultKit)
                p.getInventory().addItem(item);
        }
    }

    public static void clearInv(Player p) {
        p.setHealth(p.getMaxHealth()); //Устанавливаем полное хп
        p.setFoodLevel(20); //Убираем голод
        p.setLevel(0); //Уровень опыта ноль
        p.setExp(0); //Уровень опыта ноль
        p.setGameMode(GameMode.SURVIVAL); //Режим приключенченский (нельзя ломать)
        p.setAllowFlight(false); //Нельзя летать
        p.setFlying(false); //Убираем флай
        for (PotionEffect effect : p.getActivePotionEffects()) //Убираем эффекты
            p.removePotionEffect(effect.getType());
        p.getInventory().clear(); //Чистим инвентарь
        p.getInventory().setArmorContents(null); //Очищаем ячейки брони
    }
}
