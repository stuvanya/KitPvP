package ru.elliot789.kitpvp;

import ru.elliot789.eapi.eAPI;
import ru.elliot789.eapi.tab.NametagHandler;
import ru.elliot789.eapi.tab.NametagManager;
import ru.elliot789.eapi.tab.api.NametagAPI;
import ru.elliot789.eapi.tab.eTab;

public class Tab {


    public static void TabEnable() {

        eTab.manager = new NametagManager(eAPI.plugin);
        eTab.handler = new NametagHandler(eTab.manager);

        if (eTab.getApi() == null) {
            eTab.api = new NametagAPI(eTab.handler, eTab.manager);
        }
    }

    public static void onDisable() {
        eTab.manager.reset();
    }
}
